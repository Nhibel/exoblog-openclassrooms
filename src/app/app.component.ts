import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'exoBlog-ocr';

  posts = [
    {
      title: 'JCVD Post 1',
      content: 'Oui alors écoute moi, ce n\'est pas un simple sport car là, j\'ai un chien en ce moment à côté de moi et je le caresse, parce que spirituellement, on est tous ensemble, ok ? Pour te dire comme on a beaucoup à apprendre sur la vie ! ',
      loveIts: 0,
      created_at: new Date().toISOString()
    },
    {
      title: 'JCVD Post 2',
      content: 'Tu vois, je suis mon meilleur modèle car en vérité, la vérité, il n\'y a pas de vérité et cette officialité peut vraiment retarder ce qui devrait devenir... Pour te dire comme on a beaucoup à apprendre sur la vie ! ',
      loveIts: 0,
      created_at: new Date().toISOString()
    }
  ]
}
