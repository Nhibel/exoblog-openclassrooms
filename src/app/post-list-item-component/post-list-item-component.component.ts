import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {
  
  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveIts: number;
  // @Input('postCreatedAt') date : Date;  
  @Input() postCreatedAt : Date;    
  
  constructor() { }
  
  ngOnInit() {
  }
  
  love: number = 0;

  // postCreatedAt:number = Date.now();

  onLoveIt() {
    this.love++;
  }

  onDontLoveIt() {
    this.love--;
  }
}
